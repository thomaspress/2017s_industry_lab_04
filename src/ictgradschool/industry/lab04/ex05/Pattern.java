package ictgradschool.industry.lab04.ex05;

/**
 * Created by tpre939 on 15/11/2017.
 */
public class Pattern {
    private int numOfChar;
    private char aChar;

    public Pattern(int numOfChar, char aChar){
        this.numOfChar = numOfChar;
        this.aChar = aChar;
    }

    public String toString(){
        String completeLine = "";
        for (int i = 0 ;i < numOfChar;i++){
            completeLine += aChar;
        }
        return completeLine;
    }

    public void setNumberOfCharacters(int numOfChar){
        //set number of characters
        this.numOfChar = numOfChar;
    }
    public int getNumberOfCharacters(){
        return numOfChar;
    }
}
