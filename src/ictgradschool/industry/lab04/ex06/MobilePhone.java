package ictgradschool.industry.lab04.ex06;

import java.util.Objects;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    private String brand = "";
    private String model = "";
    private double price = 0.0;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel(){
        return model;
    }
    
    // TODO Insert setModel() method here
    public void setModel(String model){
        this.model = model;
    }
    // TODO Insert getPrice() method here
    public double getPrice(){
        return price;
    }
    // TODO Insert setPrice() method here
    public void setPrice(double price){
        this.price = price;
    }
    // TODO Insert toString() method here
    public String toString(){
        return getBrand() + " " + getModel() + " which cost " + "$" + getPrice();
    }
    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone other){
        if (this.price < other.price){
            return true;
        }
        return false;
    }
    // TODO Insert equals() method here
    public boolean equals(Object other){
        if (other instanceof MobilePhone){
            //This line casts the incoming object as an class object
            MobilePhone otherPhone = (MobilePhone) other;
            return this.price == otherPhone.price
                    && this.brand.equals(otherPhone.brand)
                    && this.model.equals(otherPhone.model);

        }
        return false;
    }
}


