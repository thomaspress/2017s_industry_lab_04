package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    private int userChoice = 0;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.println("Hi! What is your name?");
        String nameInput = Keyboard.readInput();
//            while (nameInput.equals("")){
//                System.out.println("Invalid input");
//                start();
//            }
        //label while loop
        outerloop:
        while (userChoice != 4) {
            System.out.println("1. Rock");
            System.out.println("2. Scissors");
            System.out.println("3. Paper");
            System.out.println("4. Quit");
            System.out.println("Enter choice: ");
            int inputChoice = Integer.parseInt(Keyboard.readInput());

            switch (inputChoice) {
                case 1:
                    userChoice = ROCK;
                    break;
                case 2:
                    userChoice = SCISSORS;
                    break;
                case 3:
                    userChoice = PAPER;
                    break;
                case 4:
                    userChoice = 4;
                    break outerloop;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
            displayPlayerChoice(nameInput, userChoice);
            int computerChoice = (int) (Math.random() * 3 + 1);
            displayPlayerChoice("Computer", computerChoice);
            if (userChoice == computerChoice) {
                System.out.println("No one wins");
            } else if
                    (userWins(userChoice, computerChoice)) {
                System.out.println(nameInput + " wins because " + getResultString(userChoice, computerChoice));
            } else {
                System.out.println("Computer wins because " + getResultString(userChoice, computerChoice));
            }
        }
        System.out.println("Goodbye " + nameInput + ". Thanks for playing :)");
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        switch (choice){
            case ROCK:
                System.out.println(name + " chose rock.");
                break;
            case SCISSORS:
                System.out.println(name + " chose scissors.");
                break;
            case PAPER:
                System.out.println(name + " chose paper.");
                break;
        }

    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        //return ((computerChoice < playerChoice)||(computerChoice==PAPER && playerChoice == SCISSORS)){
            return ((computerChoice==ROCK && playerChoice==PAPER)||(computerChoice==PAPER && playerChoice == SCISSORS)||(computerChoice==SCISSORS&&playerChoice==ROCK));

    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = "you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if ((computerChoice == PAPER && playerChoice == ROCK)||(playerChoice == PAPER && computerChoice == ROCK)) {
            return PAPER_WINS;
        } else if
                ((computerChoice == ROCK && playerChoice == SCISSORS)||(playerChoice == ROCK && computerChoice == SCISSORS)) {
            return ROCK_WINS;
        } else if ((computerChoice == SCISSORS && playerChoice == PAPER)||(playerChoice == SCISSORS && computerChoice == PAPER)) {
            return SCISSORS_WINS;
        } else {
            return TIE;
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
