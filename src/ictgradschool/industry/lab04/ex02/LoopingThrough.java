package ictgradschool.industry.lab04.ex02;

public class LoopingThrough {
    private double[] heights = new double[5];

    public void startArray(){
        for (int i = 0; i < 5; i++){
            int x = (int)(Math.random()*(200-149))+150;
            heights[i] = x;
        }
        //System.out.println(this.toString());
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < heights.length; i++){
            output += heights[i] +" ";
        }
        return output;
    }

    public static void main(String[] args){
        LoopingThrough run = new LoopingThrough();
        run.startArray();
        System.out.println(run.toString());
    }

}
